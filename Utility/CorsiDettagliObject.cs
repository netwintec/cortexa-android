﻿using System;
namespace Cortexa
{
	public class CorsiDettagliObject
	{
		public string id { get; set; }
		public string numero { get; set; }
		public string titolo { get; set; }
		public string descrizione { get; set; }
		public string requisiti { get; set; }
		public string url_iscrizione { get; set; }
	}
}

