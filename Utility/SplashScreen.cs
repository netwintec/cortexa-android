﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Cortexa
{
	[Activity(MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash2", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashScreen : Activity
	{
		bool notification;
		string type;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);


			notification = Intent.GetBooleanExtra("Notification", false);
			type = Intent.GetStringExtra("type");
			//conv_id = Intent.GetStringExtra("key");

			SetContentView(Resource.Layout.SplashScreen);
			ThreadPool.QueueUserWorkItem(o => LoadActivity());
		}

		public void LoadActivity()
		{
			Thread.Sleep(1000);
			RunOnUiThread(() =>
			{
				var intent = new Intent(this, typeof(MainActivity));

				if (notification)
				{

				intent.PutExtra("type", type);
				//intent.PutExtra("key", conv_id);
				intent.PutExtra("Notification", true);

				}

				StartActivity(intent);
				Finish();
			});
		}
	}
}

