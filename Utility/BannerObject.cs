﻿using System;
using Android.Graphics;

namespace Cortexa
{
	public class BannerObject
	{
		public string id { get; set; }
		public string url { get; set; }

		public Bitmap image;
	}
}

