﻿using System.Text;
using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Util;
using Gcm.Client;
using RestSharp;

[assembly: UsesPermission (Android.Manifest.Permission.ReceiveBootCompleted)]

namespace Cortexa
{
	//You must subclass this!
	[BroadcastReceiver(Permission=Constants.PERMISSION_GCM_INTENTS)]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new string[] { "@PACKAGE_NAME@" })]

	public class GcmBroadcastReceiver : GcmBroadcastReceiverBase<PushHandlerService>
	{
		//IMPORTANT: Change this to your own Sender ID!
		//The SENDER_ID is your Google API Console App Project ID.
		//  Be sure to get the right Project ID from your Google APIs Console.  It's not the named project ID that appears in the Overview,
		//  but instead the numeric project id in the url: eg: https://code.google.com/apis/console/?pli=1#project:785671162406:overview
		//  where 785671162406 is the project id, which is the SENDER_ID to use!
		public static string[] SENDER_IDS = new string[] {"293990887859"};

		public const string TAG = "PushSharp-GCM";
	}

	[Service] //Must use the service tag
	public class PushHandlerService : GcmServiceBase
	{
		public PushHandlerService() : base(GcmBroadcastReceiver.SENDER_IDS) { }

		const string TAG = "CORTEXA-GCM";

		protected override void OnRegistered (Context context, string registrationId)
		{
			Log.Verbose(TAG, "GCM Registered: " + registrationId);

			MainActivity.Instance.GCMToken = registrationId;

			if (MainActivity.Instance.Login)
			{

				string token = MainActivity.Instance.prefs.GetString("TokenCortexa", "");

				var client = new RestClient("http://www.cortexa.it/");

				var request = new RestRequest("cortexa-app/tokenpush.php?token=" + token + "&device=android&push=" + MainActivity.Instance.GCMToken, Method.GET);


				client.ExecuteAsync(request, (s, e) =>
				{

					Console.WriteLine("CGM RESULT:" + s.Content + "|" + s.StatusCode);

				});

				MainActivity.Instance.PushSend = true;
			}

			//Eg: Send back to the server
			//	var result = wc.UploadString("http://your.server.com/api/register/", "POST", 
			//		"{ 'registrationId' : '" + registrationId + "' }");

			//MainActivity.Instance.PushToken = registrationId;

		}

		protected override void OnUnRegistered (Context context, string registrationId)
		{
			
			Log.Verbose(TAG, "GCM Unregistered: " + registrationId);

		}

		protected override void OnMessage (Context context, Intent intent)
		{
			Log.Info(TAG, "GCM Message Received!");

			var msg = new StringBuilder();  

			if (intent != null && intent.Extras != null)
			{
				foreach (var key in intent.Extras.KeySet())
					msg.AppendLine(key + "=" + intent.Extras.Get(key).ToString());
			}

			Console.WriteLine ("message:\n"+msg);

			string Message = "";
			string Type = "";
			try{
				Message = intent.Extras.Get("message").ToString();
				Type = intent.Extras.Get("type").ToString();
			}catch(Exception e){
				//reveal = true;
			}

			bool close2;
			try{
				close2 = MainActivity.Instance.IsFinishing;
			}catch(Exception e){
				close2 = true;
			}

			//MainActivity.Instance.Notification

			Console.WriteLine ("2:"+close2);
			createNotification(Message,close2,Type);

			
		}

		protected override bool OnRecoverableError (Context context, string errorId)
		{
			Log.Warn(TAG, "Recoverable Error: " + errorId);
			return base.OnRecoverableError (context, errorId);
		}

		protected override void OnError (Context context, string errorId)
		{
			Log.Error(TAG, "GCM Error: " + errorId);
		}

		void createNotification(string title,bool close2,string type)
		{

			Console.WriteLine ("cN:"+title +"|"+close2+"|"+type);

			var manager =
				(NotificationManager)this.ApplicationContext.GetSystemService(Context.NotificationService);

			Intent intent=null;

			if (close2) {

				var intent2 = ApplicationContext.PackageManager.GetLaunchIntentForPackage(this.ApplicationContext.PackageName);
				intent = new Intent (this, typeof(SplashScreen));
				intent.PutExtra ("Notification",true);
				intent.PutExtra("type", type);

			} else {

				intent = new Intent (this, typeof(NotificationActivity));
				intent.PutExtra("type", type);
				intent.AddFlags (ActivityFlags.ClearTop);


					

			}

			var pendingIntent = PendingIntent.GetActivity(this.ApplicationContext, 0, intent, PendingIntentFlags.UpdateCurrent);

			var builder = new Notification.Builder(this.ApplicationContext)
				.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
			    .SetSmallIcon(Resource.Mipmap.notification)
				.SetContentTitle("Cortexa")
				.SetContentText (title)
				.SetContentIntent(pendingIntent)
				.SetAutoCancel(true);




			manager.Notify(1, builder.Build());

		}
	}
}

