﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using RestSharp;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentSondaggioHome : Fragment
	{

		RelativeLayout Load, NoSon, Son;

		//TextView tit;

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentSondaggioHomeLayout, container, false);

			Typeface osr = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Regular.ttf");
			Typeface osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");

			Load = view.FindViewById<RelativeLayout>(Resource.Id.LoadView);
			NoSon = view.FindViewById<RelativeLayout>(Resource.Id.NoSonView);
			Son = view.FindViewById<RelativeLayout>(Resource.Id.SonView);

			Load.Visibility = ViewStates.Visible;
			NoSon.Visibility = ViewStates.Gone;
			Son.Visibility = ViewStates.Gone;

			view.FindViewById<TextView>(Resource.Id.NoSonText).Typeface = osr;
			//tit = view.FindViewById<TextView>(Resource.Id.Title);
			//tit.Typeface = osr;
			view.FindViewById<TextView>(Resource.Id.StartButton).Typeface = osr;
			view.FindViewById<TextView>(Resource.Id.StartButton).Click += delegate { 
			
				FragmentSondaggio fragment = new FragmentSondaggio();

				FragmentManager frgManager = this.FragmentManager;
				frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
					.AddToBackStack(null)
					.Commit();

			};

			view.FindViewById<ProgressBar>(Resource.Id.Load).IndeterminateDrawable.SetColorFilter(Color.Rgb(78,78,78), PorterDuff.Mode.Multiply);

			LoadSondaggi();

			return view;
		}


		public void LoadSondaggi()
		{
			//***** DOWNLOAD SONDAGGIO ****
			var clientSondaggio = new RestClient("http://www.cortexa.it/");

			var requestSondaggio = new RestRequest("cortexa-app/sondaggi.php?token=" + MainActivity.Instance.prefs.GetString("TokenCortexa",""), Method.GET);


			clientSondaggio.ExecuteAsync(requestSondaggio, (s, e) =>
			{

				Console.WriteLine("Result Sondaggio:" + s.StatusCode + "|" + s.Content);

				if (s.StatusCode == System.Net.HttpStatusCode.OK)
				{

					MainActivity.Instance.ListSondaggio = JsonConvert.DeserializeObject<List<SondaggioObject>>(s.Content);

					Activity.RunOnUiThread(() =>
					{

						Load.Visibility = ViewStates.Gone;
						NoSon.Visibility = ViewStates.Gone;
						Son.Visibility = ViewStates.Visible;

						//tit.Text = "";MainActivity.Instance.ListSondaggio[0].title;
					});

				}

				else
				{
					Activity.RunOnUiThread(() =>
					{
						Load.Visibility = ViewStates.Gone;
						NoSon.Visibility = ViewStates.Visible;
						Son.Visibility = ViewStates.Gone;
					});
				}
			});
		}
	}
}

