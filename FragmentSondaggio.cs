﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using RestSharp;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentSondaggio : Fragment
	{
		List<SondaggioObject> ListSondaggio;
		TextView Domanda,Counter,Titolo;

		LinearLayout RisposteView;
		RelativeLayout LoadView;

		Button Avanti;

		public int indiceDomanda = 0;

		List<ImageView> ListImage = new List<ImageView>();
		List<bool> ListImageBool = new List<bool>();

		List<AssociazioneObject> ListResponse = new List<AssociazioneObject>();

		int Acceso = Resource.Drawable.Acceso;
		int Spento = Resource.Drawable.Spento;

		LayoutInflater inf;
		ViewGroup cont;

		bool Clicked = false;
		int Selected = 0;

		Typeface osr ;
		Typeface osb ;


		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentSondaggioLayout, container, false);

			ListSondaggio = MainActivity.Instance.ListSondaggio;

			osr = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Regular.ttf");
			osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");

			inf = inflater;
			cont = container;

			RisposteView = view.FindViewById<LinearLayout>(Resource.Id.RisposteView);
			LoadView = view.FindViewById<RelativeLayout>(Resource.Id.LoadView);
			Titolo = view.FindViewById<TextView>(Resource.Id.Titolo);
			Domanda = view.FindViewById<TextView>(Resource.Id.Domanda);
			Counter = view.FindViewById<TextView>(Resource.Id.Counter);
			Avanti = view.FindViewById<Button>(Resource.Id.SendButton);

			LoadView.Visibility = ViewStates.Gone;

			Titolo.Typeface = osr;
			Domanda.Typeface = osr;
			Counter.Typeface = osr;
			Avanti.Typeface = osr;

			Avanti.Click += delegate
			{
				if (Clicked)
				{
					if ((indiceDomanda + 1) < ListSondaggio.Count)
					{

						AssociazioneObject ao = new AssociazioneObject();
						ao.sppolls_poll_id = ListSondaggio[indiceDomanda].sppolls_poll_id;
						ao.poll = ListSondaggio[indiceDomanda].polls[Selected].poll;

						ListResponse.Add(ao);


						RisposteView.RemoveAllViews();
						indiceDomanda++;
						CreateView();

					}
					else
					{
						if ((indiceDomanda + 1) == ListSondaggio.Count)
						{
							AssociazioneObject ao = new AssociazioneObject();
							ao.sppolls_poll_id = ListSondaggio[indiceDomanda].sppolls_poll_id;
							ao.poll = ListSondaggio[indiceDomanda].polls[Selected].poll;

							ListResponse.Add(ao);

							indiceDomanda++;
						}

						if ((indiceDomanda) == ListSondaggio.Count)
						{

							LoadView.Visibility = ViewStates.Visible;
							SendResponse();
						}
					}
				}
			};

			CreateView();

			view.FindViewById<ProgressBar>(Resource.Id.Load).IndeterminateDrawable.SetColorFilter(Color.White, PorterDuff.Mode.Multiply);

			return view;
		}

		private int ConvertDpToPixel(float dp)
		{
			var pixel = (int)(dp * Resources.DisplayMetrics.Density);
			return pixel;
		}

		public void CreateView() {

			Clicked = false;
			ListImage.Clear();
			ListImageBool.Clear();

			Domanda.Text = ListSondaggio[indiceDomanda].title;
			Counter.Text = (indiceDomanda + 1) + "/" + ListSondaggio.Count;

			if ((indiceDomanda + 1) == ListSondaggio.Count)
				Avanti.Text = "Invia Sondaggio";

			for (int i = 0; i < ListSondaggio[indiceDomanda].polls.Count; i++)
			{

				var a = ListSondaggio[indiceDomanda].polls[i];

				View RView = inf.Inflate(Resource.Layout.RispostaObjectLayout, cont, false);

				RView.FindViewById<TextView>(Resource.Id.textView1).Text = a.poll;
				RView.FindViewById<TextView>(Resource.Id.textView1).Typeface =osr;

				ListImage.Add(RView.FindViewById<ImageView>(Resource.Id.imageView1));
				ListImageBool.Add(false);

				int indice = i;

				RView.Click += delegate
				{

					Console.WriteLine(indice + "!" + i);

					/*NewsSelected = ListNews[indice];

					FragmentNewsDettaglio fragment = new FragmentNewsDettaglio();

					FragmentManager frgManager = this.FragmentManager;
					frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();
					*/

					if (!ListImageBool[indice])
					{
						for (int ii = 0; ii < ListImage.Count; ii++)
						{
							if (ListImageBool[ii])
							{
								ListImage[ii].SetImageResource(Spento);
								ListImageBool[ii] = false;
							}
						}

						ListImage[indice].SetImageResource(Acceso);
						ListImageBool[indice] = true;

						Clicked = true;

						Selected = indice;
					}

				};

				RisposteView.AddView(RView);

			}

		}

		public void SendResponse()
		{
			int finiti = 0;
			int ErrFiniti = 0;

			foreach (var a in ListResponse)
			{
				//***** DOWNLOAD SONDAGGIO ****
				var clientSondaggio = new RestClient("http://www.cortexa.it/");

				var requestSondaggio = new RestRequest("cortexa-app/sondaggi_set.php?token=" + MainActivity.Instance.prefs.GetString("TokenCortexa","") + "&id=" + a.sppolls_poll_id + "&risposta=" + a.poll, Method.GET);

				//sondaggi_set.php?token=c839740856c9dd291bc01058facb1b4a&id=1&risposta=Risposta 2

				clientSondaggio.ExecuteAsync(requestSondaggio, (s, e) =>
				{

					Console.WriteLine("Result Sondaggio:" + s.StatusCode + "|" + s.Content);

					if (s.StatusCode == System.Net.HttpStatusCode.OK)
					{

						finiti++;

					}

					else
					{
						finiti++;
						ErrFiniti++;
					}

					if ((finiti + 1) == ListResponse.Count)
					{
						Activity.RunOnUiThread(() =>
						{
							if ((ErrFiniti + 1) == ListResponse.Count)
							{
								//FinishError();
							}
							else
							{
								Finish();
							}
						});
					}
				});
			}

		}


		public void Finish() { 
		
			AlertDialog.Builder alert = new AlertDialog.Builder(Activity);
			alert.SetTitle("Sondaggio");
			alert.SetMessage("Sondaggio completato con successo");
			alert.SetPositiveButton("OK", (senderAlert, args) =>
			{

				Activity.SupportFragmentManager.PopBackStackImmediate();

			});

			//fa partire l'alert su di un trhead
			Activity.RunOnUiThread(() =>
			{
				alert.Show();
			});
		}
	}
}

