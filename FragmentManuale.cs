﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Webkit;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentManuale : Fragment
	{
		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentManualeLayout, container, false);

			//****************** PDF READER **************

			WebView webview = view.FindViewById<WebView>(Resource.Id.webView1);
			webview.Settings.JavaScriptEnabled = true;
			webview.Settings.PluginsEnabled = true;
			webview.LoadUrl("https://docs.google.com/gview?embedded=true&url="+MainActivity.Instance.ListManuale[0].url);

			webview.SetWebViewClient(new WebViewClient());
			webview.SetWebChromeClient(new WebChromeClient());

			return view;
			//return base.OnCreateView(inflater, container, savedInstanceState);
		}
	}
}

