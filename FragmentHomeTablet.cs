﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentHomeTablet : Fragment
	{
		ViewFlipper immagineSwipe;

		List<BannerObject> ListBanner;

		public Timer timer;

		public LinearLayout l1;

		public static FragmentHomeTablet Instance { private set; get;}

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentHomeTablet, container, false);

			FragmentHomeTablet.Instance = this;


			Typeface osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");

			ListBanner = MainActivity.Instance.ListBanner;

			l1 = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);

			immagineSwipe = view.FindViewById<ViewFlipper>(Resource.Id.immagineSwipe);

			for (int i = 0; i < ListBanner.Count; i++)
			{
				BannerObject bo = ListBanner[i];
				immagineSwipe.AddView(insertPhoto(bo.url, i,bo));
			}

			timer = new Timer();
			timer.Interval = 3000;
			timer.Elapsed += (sender, e) => {

				Activity.RunOnUiThread(() => { 
					immagineSwipe.ShowNext();
				});

			};
			timer.Enabled = true;
			timer.Start();

			ImageView ConsorzioButton = view.FindViewById<ImageView>(Resource.Id.ConsorzioButton);
			ImageView NewsButton = view.FindViewById<ImageView>(Resource.Id.NewsButton);
			ImageView VideoButton = view.FindViewById<ImageView>(Resource.Id.VideoButton);
			ImageView WebinarButton = view.FindViewById<ImageView>(Resource.Id.WebinarButton);
			ImageView PartnerButton = view.FindViewById<ImageView>(Resource.Id.PartnerButton);
			ImageView ManualeButton = view.FindViewById<ImageView>(Resource.Id.ManualeButton);

			TextView ConsorzioText = view.FindViewById<TextView>(Resource.Id.ConsorzioText);
			TextView NewsText = view.FindViewById<TextView>(Resource.Id.NewsText);
			TextView VideoText = view.FindViewById<TextView>(Resource.Id.VideoText);
			TextView WebinarText = view.FindViewById<TextView>(Resource.Id.WebinarText);
			TextView PartnerText = view.FindViewById<TextView>(Resource.Id.PartnerText);
			TextView ManualeText = view.FindViewById<TextView>(Resource.Id.ManualeText);

			ConsorzioText.TextSize = 15;
			ConsorzioText.Typeface = osb;

			NewsText.TextSize = 15;
			NewsText.Typeface = osb;

			VideoText.TextSize = 15;
			VideoText.Typeface = osb;

			WebinarText.TextSize = 15;
			WebinarText.Typeface = osb;

			PartnerText.TextSize = 15;
			PartnerText.Typeface = osb;

			ManualeText.TextSize = 15;
			ManualeText.Typeface = osb;

			ConsorzioButton.Click += delegate
			{
				ClickListener(0);
			};

			NewsButton.Click += delegate
			{
				ClickListener(1);
			};

			VideoButton.Click += delegate
			{
				ClickListener(2);
			};

			WebinarButton.Click += delegate
			{
				ClickListener(3);
			};

			PartnerButton.Click += delegate
			{
				ClickListener(4);
			};

			ManualeButton.Click += delegate
			{
				ClickListener(5);
			};

			IWindowManager windowManager = Android.App.Application.Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();

			var rotation = windowManager.DefaultDisplay.Rotation;
			bool isLandscape = rotation == SurfaceOrientation.Rotation0 || rotation == SurfaceOrientation.Rotation180;

			Console.WriteLine("ORIENTATION:"+isLandscape);
			if(isLandscape)
				removeBanner(true);


			return view;
			//return base.OnCreateView(inflater, container, savedInstanceState);
		}

		public View insertPhoto(String path, int indice,BannerObject obj)
		{
			//Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);

			float density = Resources.DisplayMetrics.Density;
			LinearLayout layout = new LinearLayout(Application.Context);
			layout.LayoutParameters = new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
			layout.SetGravity(GravityFlags.Center);

			ImageView imageView = new ImageView(Application.Context);
			imageView.LayoutParameters = new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
			imageView.SetImageResource(Resource.Drawable.LogoPiccolo);
			imageView.SetAdjustViewBounds(true);

			DisplayMetrics metrics = new DisplayMetrics();
			Activity.WindowManager.DefaultDisplay.GetMetrics(metrics);

			Console.WriteLine(metrics.WidthPixels+"|"+metrics.HeightPixels);
			int px;
			if (metrics.WidthPixels > metrics.HeightPixels)
				px = metrics.HeightPixels;
			else 
				px = metrics.WidthPixels;
			
			imageView.SetMaxWidth(px + 150);
			//imageView.SetScaleType(ImageView.ScaleType.CenterInside);

			imageView.Click += delegate
			{

			};

			if (obj.image == null)
				caricaImmagineAsync(path, imageView, obj);
			else
				imageView.SetImageBitmap(obj.image);
			
			layout.AddView(imageView);

			return layout;
		}

		private int ConvertDpToPixel(float dp)
		{
			var pixel = (int)(dp * Resources.DisplayMetrics.Density);
			return pixel;
		}

		async void caricaImmagineAsync(String uri, ImageView immagine_view, BannerObject obj)
		{
			WebClient webClient = new WebClient();
			byte[] bytes = null;
			try
			{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch (TaskCanceledException)
			{
				Console.WriteLine("Task Canceled!**************************************");
				return;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap(immagine);
			obj.image = immagine;

			Console.WriteLine("Immagine caricata!");

		}

		public void ClickListener(int button)
		{
			Fragment fragment = null;
			bool isFragment = false;

			switch (button)
			{
				case 0:
					fragment = new FragmentConsorzio();
					isFragment = true;
					break;
				case 1:
					fragment = new FragmentNews();
					isFragment = true;
					break;
				case 2:
					fragment = new FragmentVideo();
					isFragment = true;
					break;
				case 3:
					fragment = new FragmentCorsi();
					isFragment = true;
					break;
				case 4:
					fragment = new FragmentPartners();
					isFragment = true;
					break;
				case 5:
					fragment = new FragmentManuale();
					isFragment = true;
					break;
				default:
					break;
			}
			if (isFragment)
			{

				timer.Stop();

				FragmentManager frgManager = this.FragmentManager;
				frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
					.AddToBackStack(null)
					.Commit();
			}
		}

		public void removeBanner(bool land) {

			if (land)
			{
				l1.Visibility = ViewStates.Gone;
			}
			else{
				l1.Visibility = ViewStates.Visible;
			}

		}
	}
}

