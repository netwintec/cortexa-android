﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using RestSharp;

namespace Cortexa
{
	[Activity(Label = "HomePageActivityTablet", Theme = "@style/AppTheme", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize)]
	public class HomePageActivityTablet : BaseActivity
	{
		
		private DrawerLayout mDrawerLayout;
		private ListView mDrawerList;

		//List<DrawerItem> dataList;
		DrawerAdapter adapter;

		public ImageView menuIcon;
		public ImageView backIcon;

		public FragmentManager.IOnBackStackChangedListener backStackListener;
		ISharedPreferences prefs;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			setActionBarIcon(Resource.Drawable.MenuSpento, Resource.Drawable.Back);



			//Set default settings
			//PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

			// Set Navigation Drawer
			//dataList = new ArrayList<>();
			mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView>(Resource.Id.left_drawer);
			mDrawerList.SetFooterDividersEnabled(false);
			mDrawerList.SetHeaderDividersEnabled(false);
			mDrawerList.Divider.SetAlpha(0);

			//mDrawerList.Adapter = new ArrayAdapter<string> (this, Resource.Layout.item_menu, Section);
			adapter = new DrawerAdapter(this);

			mDrawerList.SetAdapter(adapter);

			menuIcon = (ImageView)getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView)getToolbar().FindViewById(Resource.Id.back_icon);
			backIcon.Visibility = ViewStates.Invisible;

			menuIcon.Click += delegate
			{
				if (mDrawerLayout.IsDrawerOpen(GravityCompat.Start))
				{
					mDrawerLayout.CloseDrawer(GravityCompat.Start);
					//	menuIcon.SetImageResource(Resource.Drawable.menu_toolbar);
				}
				else {
					mDrawerLayout.OpenDrawer(GravityCompat.Start);
					//	menuIcon.SetImageResource(Resource.Drawable.menu_toolbar_att);
				}
			};
			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

			backIcon.Click += delegate
			{
				Console.WriteLine("INFO" + (SupportFragmentManager.BackStackEntryCount).ToString());
				if (SupportFragmentManager.BackStackEntryCount == 0)
				{

				}
				else {
					//int i=SupportFragmentManager.BackStackEntryCount;
					//while(i>0)
					SupportFragmentManager.PopBackStack();
					//if(FragmentBeaconImage.player !=null){
					//	FragmentBeaconImage.player.Release();
					//}
				}
			};

			mDrawerList.ItemClick += DrawerListItemClick;
			mDrawerLayout.DrawerClosed += delegate
			{
				//menuIcon.SetImageResource(Resource.Drawable.menu_toolbar);
			};
			mDrawerLayout.DrawerOpened += delegate
			{
				//menuIcon.SetImageResource(Resource.Drawable.menu_toolbar_att);
			};

			//Initialize fragment
			Fragment fragment;
			//Bundle args = new Bundle();
			fragment = new FragmentHomeTablet();
			//args.putString(FragmentHome.ITEM_NAME, dataList.get(1).getItemName());
			//fragment.setArguments(args);
			FragmentManager frgManager = SupportFragmentManager;
			frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
				.Commit();

			SupportFragmentManager.BackStackChanged += delegate
			{
				Console.WriteLine("INFO" + (SupportFragmentManager.BackStackEntryCount).ToString());
				if (SupportFragmentManager.BackStackEntryCount == 0)
				{
					backIcon.Visibility = ViewStates.Invisible;
				}
				else {
					backIcon.Visibility = ViewStates.Visible;
					//backIcon.SetImageResource(Resource.Drawable.back_icon);
				}
			};

			bool notification = Intent.GetBooleanExtra("Notification", false);

			if (notification)
			{

				string type = "";
				type = Intent.GetStringExtra("type");

				if (type != "")
				{
					Fragment fragmentNot = null;

					Console.WriteLine(type);

					if (type == "sondaggio")
						fragmentNot = new FragmentSondaggioHome();
					if (type == "news")
						fragmentNot = new FragmentNews();
					if (type == "manuale")
						fragmentNot = new FragmentManuale();
					if (type == "corso")
						fragmentNot = new FragmentCorsi();
					if (type == "video")
						fragmentNot = new FragmentVideo();
					

					FragmentManager frgManagerNot = SupportFragmentManager;
					frgManagerNot.BeginTransaction()
						.Replace(Resource.Id.content_frame, fragmentNot)
						.AddToBackStack(null)
						.Commit();
				}

			}

			//********** SEND NOTIFICATION TOKEN ******************

			if (!MainActivity.Instance.PushSend)
			{
				if (!string.IsNullOrWhiteSpace(MainActivity.Instance.GCMToken))
				{

					string token = MainActivity.Instance.prefs.GetString("TokenCortexa", "");

					var client = new RestClient("http://www.cortexa.it/");

					var request = new RestRequest("cortexa-app/tokenpush.php?token=" + token + "&device=android&push=" + MainActivity.Instance.GCMToken, Method.GET);


					client.ExecuteAsync(request, (s, e) =>
					{
						Console.WriteLine("HOMEPAGE RESULT:" + s.Content + "|" + s.StatusCode);
					});

					MainActivity.Instance.PushSend = true;
				}
			}

			//************************************************/


		}

		protected override int getLayoutResource()
		{
			return Resource.Layout.HomePageLayout;
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
				//case Resource.Id.home:
				//	mDrawerLayout.OpenDrawer(GravityCompat.Start);
				//	return true;
			}

			return base.OnOptionsItemSelected(item);
		}

		public override void OnBackPressed()
		{
			Console.WriteLine("INFO" + (SupportFragmentManager.BackStackEntryCount).ToString());
			if (mDrawerLayout.IsDrawerOpen(GravityCompat.End))
			{
				mDrawerLayout.CloseDrawer(GravityCompat.End);
			}
			else {
				if (SupportFragmentManager.BackStackEntryCount == 0)
				{
					//LogOut();
				}
			}
		}

		public void LogOut()
		{
			//Alert vuoi sloggarti?

			//MainActivity.Instance.CountBluetooth = -1;

			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.SetTitle("Sicuro di voler effettuare il logout?");
			alert.SetPositiveButton("Logout", (senderAlert, args) =>
			{


				var prefs = MainActivity.Instance.prefs;

				//********** DELETE NOTIFICATION TOKEN ******************
				var client = new RestClient("http://www.cortexa.it/");

				var request = new RestRequest("cortexa-app/logout.php?token=" + prefs.GetString("TokenCortexa", ""), Method.GET);

				client.ExecuteAsync(request, (s, e) =>
				{

					Console.WriteLine("Logout" + s.Content + "!" + s.StatusCode);

				});

				Console.WriteLine("2");
				//Console.WriteLine("Result:" + response.Content);

				//************************************************

				var prefEditor = prefs.Edit();
				prefEditor.PutString("TokenCortexa", "");
				prefEditor.Commit();

				Finish();

			});
			alert.SetNegativeButton("Annulla", (senderAlert, args) =>
			{
				//volendo fa qualcosa
			});
			//fa partire l'alert su di un trhead
			RunOnUiThread(() =>
			{
				alert.Show();
			});
		}

		public void DrawerListItemClick(object sender, AdapterView.ItemClickEventArgs item)
		{
			selectItem(item.Position);
		}

		public void selectItem(int position)
		{
			Fragment fragment = null;
			Bundle args = new Bundle();
			bool isFragment = false;

			switch (position)
			{
				case 0:
					if (SupportFragmentManager.BackStackEntryCount == 1)
					{
						SupportFragmentManager.PopBackStack();
					}
					if (SupportFragmentManager.BackStackEntryCount == 2)
					{
						SupportFragmentManager.PopBackStackImmediate();
						SupportFragmentManager.PopBackStackImmediate();
					}
					break;

				case 1:
					fragment = new FragmentConsorzio();
					isFragment = true;
					break;
				case 2:
					fragment = new FragmentNews();
					isFragment = true;
					break;

				case 3:
					fragment = new FragmentVideo();
					isFragment = true;
					break;

				case 4:
					fragment = new FragmentCorsi();
					isFragment = true;
					break;

				case 5:
					fragment = new FragmentPartners();
					isFragment = true;
					break;

				case 6:
					fragment = new FragmentManuale();
					isFragment = true;
					break;

				case 7:
					fragment = new FragmentSondaggioHome();
					isFragment = true;
					break;

				case 8:
					LogOut();
					break;

				default:
					break;
			}


			if (isFragment)
			{
				int i = SupportFragmentManager.BackStackEntryCount;
				while (i > 0)
				{
					SupportFragmentManager.PopBackStackImmediate();
					i = SupportFragmentManager.BackStackEntryCount;
				}

				FragmentHomeTablet.Instance.timer.Stop();
				
				FragmentManager frgManager = SupportFragmentManager;
				frgManager.BeginTransaction()
					.Replace(Resource.Id.content_frame, fragment)
					.AddToBackStack(null)
					.Commit();


			}
			mDrawerList.SetItemChecked(position, false);
			mDrawerLayout.CloseDrawer(mDrawerList);

		}

		protected override void OnPause()
		{
			base.OnPause();

		}

		protected override void OnResume()
		{
			base.OnResume();

		}

		public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
		{
			if (newConfig.Orientation == Android.Content.Res.Orientation.Landscape)
				FragmentHomeTablet.Instance.removeBanner(true);
			if (newConfig.Orientation == Android.Content.Res.Orientation.Portrait)
				FragmentHomeTablet.Instance.removeBanner(false);
			base.OnConfigurationChanged(newConfig);
		}
	}
}


