﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	
	public class FragmentCorsi : Fragment
	{

		public List<CorsiObject> ListCorsi;

		public CorsiObject CorsoSelected;

		public static FragmentCorsi Instance { private set; get; }

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentCorsiLayout, container, false);

			FragmentCorsi.Instance = this;

			LinearLayout l1 = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);

			ListCorsi = MainActivity.Instance.ListCorsi;

			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
			lp1.SetMargins(ConvertDpToPixel(10), ConvertDpToPixel(10), ConvertDpToPixel(10), ConvertDpToPixel(10));

			LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
			lp2.SetMargins(ConvertDpToPixel(10), ConvertDpToPixel(0), ConvertDpToPixel(10), ConvertDpToPixel(10));

			for (int i = 0; i < ListCorsi.Count; i++) {

				CorsiObject co = ListCorsi[i];

				ImageView imm = new ImageView(Activity);
				if(i == 0)
					imm.LayoutParameters = lp1;
				else
					imm.LayoutParameters = lp2;

				imm.SetAdjustViewBounds(true);

				if (co.Image == null)
				{
					imm.SetImageResource(Resource.Drawable.placeholder);
					caricaImmagineAsync(co.url_image, imm, co);
				}
				else {
					imm.SetImageBitmap(co.Image);
				}

				int indice = i;

				imm.Click += delegate { 
				
					Console.WriteLine(indice + "!" + i);

					CorsoSelected = ListCorsi[indice];

					FragmentCorsiDettaglio fragment = new FragmentCorsiDettaglio();

					FragmentManager frgManager = this.FragmentManager;
					frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();

				};

				l1.AddView(imm);


			}


			return view;
		}

		private int ConvertDpToPixel(float dp)
		{
			var pixel = (int)(dp * Resources.DisplayMetrics.Density);
			return pixel;
		}

		async void caricaImmagineAsync(String uri, ImageView immagine_view, CorsiObject obj)
		{
			WebClient webClient = new WebClient();
			byte[] bytes = null;
			try
			{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch (TaskCanceledException)
			{
				Console.WriteLine("Task Canceled!**************************************");
				return;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap(immagine);
			obj.Image = immagine;

			Console.WriteLine("Immagine caricata!");

		}

	}
}

