﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Util;

namespace Cortexa
{
	[Activity(Label = "NotificationActivity",NoHistory = true,ScreenOrientation = ScreenOrientation.Portrait)]
	public class NotificationActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);


			string type = "";
			type= Intent.GetStringExtra("type");


			if (IsTabletDevice(ApplicationContext) == "tablet") { 
			
				Intent intent = null;
				intent = new Intent(this, typeof(HomePageActivityTablet));
				intent.PutExtra("Notification", true);
				intent.PutExtra("type", type);
				StartActivity(intent);

			}
			if (IsTabletDevice(ApplicationContext) == "phone")
			{
				Intent intent = null;
				intent = new Intent(this, typeof(HomePageActivity));
				intent.PutExtra("Notification", true);
				intent.PutExtra("type", type);
				StartActivity(intent);
			}

		}

		static string IsTabletDevice(Context context)
		{
			try
			{

				//
				// Compute screen size
				DisplayMetrics dm = context.Resources.DisplayMetrics;
				float screenWidth = dm.WidthPixels / dm.Xdpi;
				float screenHeight = dm.HeightPixels / dm.Ydpi;
				double size = Math.Sqrt(Math.Pow(screenWidth, 2) + Math.Pow(screenHeight, 2));

				//
				// Tablet devices should have a screen size greater than 6 inches
				if (size >= 6)
					return "tablet";
				else
					return "phone";

			}
			catch (Exception e)
			{
				return "error";
			}
		}
	}
}

