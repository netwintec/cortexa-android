﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Cortexa
{
	public class DrawerAdapter : BaseAdapter
	{
		Activity context;

		public List<ListObject> items;

		public DrawerAdapter(Activity context) : base()
		{
			this.context = context;

			//For demo purposes we hard code some data here
			this.items = new List<ListObject>() {
				new ListObject() { Logout = false, Text = "HOME",Image = Resource.Drawable.HomeMenu },
				new ListObject() { Logout = false, Text = "CONSORZIO", Image = Resource.Drawable.ConsorzioMenu },
				new ListObject() { Logout = false, Text = "NEWS", Image = Resource.Drawable.NewsMenu },
				new ListObject() { Logout = false, Text = "VIDEO", Image = Resource.Drawable.VideoMenu },
				new ListObject() { Logout = false, Text = "WEBINAR", Image = Resource.Drawable.WebinarMenu },
				new ListObject() { Logout = false, Text = "PARTNERS", Image = Resource.Drawable.PartnerMenu },
				new ListObject() { Logout = false, Text = "MANUALE", Image = Resource.Drawable.DocumentiMenu },
				new ListObject() { Logout = false, Text = "SONDAGGIO", Image = Resource.Drawable.SondaggioMenu },
				new ListObject() { Logout = true }
			};
		}

		public override int Count
		{
			get { return items.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  

			View view = convertView;


			Typeface osr = Typeface.CreateFromAsset(this.context.Assets, "fonts/OpenSans_Regular.ttf");

			ListObject dItem = this.items[position];

			if (dItem.Logout)
			{
				LayoutInflater inflater = ((Activity)context).LayoutInflater;

				view = inflater.Inflate(Resource.Layout.drawer_logout, parent, false);
				view.FindViewById<TextView>(Resource.Id.textView1).Text = "Logout";
				view.FindViewById<TextView>(Resource.Id.textView1).Typeface = osr;

				return view;

			}
			else {

				LayoutInflater inflater = ((Activity)context).LayoutInflater;

				view = inflater.Inflate(Resource.Layout.drawer_option, parent, false);

				view.FindViewById<ImageView>(Resource.Id.optionImage).SetImageResource(dItem.Image);
				view.FindViewById<TextView>(Resource.Id.optionText).Text = dItem.Text;
				view.FindViewById<TextView>(Resource.Id.optionText).Typeface = osr;

				return view;
			}

			//return view;
		}

		public ListObject GetItemAtPosition(int position)
		{
			return items[position];
		}
	}


	public class ListObject
	{
		public int Image
		{
			get;
			set;
		}

		public string Text
		{
			get;
			set;
		}

		public bool Logout
		{
			get;
			set;
		}
	}
}

