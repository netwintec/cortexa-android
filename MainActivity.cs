﻿using System;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android.Content.PM;
using Android.Net;
using Android.Graphics;
using System.Collections.Generic;
using Android.Views;
using RestSharp;
using Newtonsoft.Json;
using Android.Util;
using Gcm.Client;

namespace Cortexa
{
	[Activity(Label = "MainActivity", ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity
	{
		int count = 1;

		public string token;
		public bool FirstDownload = false;
		public List<VideoObject> ListVideo = new List<VideoObject>();
		public List<CorsiObject> ListCorsi = new List<CorsiObject>();
		public List<NewsObject> ListNews = new List<NewsObject>();
		public List<ManualeObject> ListManuale = new List<ManualeObject>();
		public List<PartnersObject> ListPartners = new List<PartnersObject>();
		public List<BannerObject> ListBanner = new List<BannerObject>();
		public List<SondaggioObject> ListSondaggio = new List<SondaggioObject>();

		int LoadFiniti = 0;
		int LoadTotali = 6;

		public ISharedPreferences prefs;

		bool notification;
		string type = "";

		public string GCMToken = "";
		public bool Login = false;
		public bool PushSend = false;

		public static MainActivity Instance { private set; get; }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			notification = Intent.GetBooleanExtra("Notification", false);
			type = Intent.GetStringExtra("type");

			GcmClient.CheckDevice(this);
			GcmClient.CheckManifest(this);
			GcmClient.Register(this, GcmBroadcastReceiver.SENDER_IDS);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			ActionBar.Hide();

			prefs = Application.Context.GetSharedPreferences("Cortexa", FileCreationMode.Private);

			Console.WriteLine("It is a "+IsTabletDevice(ApplicationContext));

			MainActivity.Instance = this;

			Console.WriteLine(Assets.ToString());

			Typeface osr = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_Regular.ttf");

			FindViewById<ProgressBar>(Resource.Id.progressBar1).IndeterminateDrawable.SetColorFilter(Color.White, PorterDuff.Mode.Multiply);
			FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

			TextView Registrati = FindViewById<TextView>(Resource.Id.RegistratiButton);
			Registrati.Typeface = osr;
			Registrati.Click += delegate
			{
				var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("http://www.cortexa.it/it/area-riservata/registrazione.html"));
				intent.AddFlags(ActivityFlags.NewTask);

				Application.Context.StartActivity(intent);

			};

			EditText Utente = FindViewById<EditText>(Resource.Id.UserText);
			EditText Password = FindViewById<EditText>(Resource.Id.PasswordText);

			Utente.Typeface = osr;
			Password.Typeface = osr;

			Button Login = FindViewById<Button>(Resource.Id.LoginButton);
			Login.Typeface = osr;
			Login.Click += delegate {

				if (String.IsNullOrWhiteSpace(Utente.Text) || String.IsNullOrWhiteSpace(Password.Text))
				{
					
					AlertDialog.Builder alert = new AlertDialog.Builder(this);
					alert.SetTitle("Login Non Riuscito");
					alert.SetMessage("E-Mail o Password non inseriti");
					alert.SetPositiveButton("OK", (senderAlert, args) => { });
					alert.Show();

				}
				else {
					FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Visible;

					var clientLogin = new RestClient("http://www.cortexa.it/");

					var requestLogin = new RestRequest("cortexa-app/login.php?user=" + Utente.Text + "&pass=" + Password.Text, Method.GET);


					clientLogin.ExecuteAsync(requestLogin, (s, e) =>
					{

						Console.WriteLine("Result Login:" + s.StatusCode + "|" + s.Content);

						if (s.StatusCode == System.Net.HttpStatusCode.OK)
						{

							if (!String.IsNullOrWhiteSpace(s.Content))
							{

								token = s.Content;

								var prefEditor = prefs.Edit();
								prefEditor.PutString("TokenCortexa", token);
								prefEditor.Commit();


								if (MainActivity.Instance.FirstDownload)
								{
									RunOnUiThread(() =>
									{
										FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

										Intent intent = null;
										string response = IsTabletDevice(ApplicationContext);
										if (response == "tablet")
											intent = new Intent(this, typeof(HomePageActivityTablet));
										if (response == "phone")
											intent = new Intent(this, typeof(HomePageActivity));
										if (response != "error")
										{
											if (notification)
											{
												intent.PutExtra("Notification", notification);
												intent.PutExtra("type",type);
											}
											StartActivity(intent);
											//Finish();
										}

									});
								}
								else {
									RunOnUiThread(() =>
									{
										FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Visible;
										LoadData();
									});
								}
							}
							else {
								RunOnUiThread(() =>
									{
										FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

										Utente.Text = "";
										Password.Text = "";
										AlertDialog.Builder alert = new AlertDialog.Builder(this);
										alert.SetTitle("Login Non Riuscito");
										alert.SetMessage("E-Mail o Password Errata");
										alert.SetPositiveButton("OK", (senderAlert, args) => { });
										alert.Show();
									});
							}
						}
						else {
							RunOnUiThread(() =>
									{
										FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

										Utente.Text = "";
										Password.Text = "";
										AlertDialog.Builder alert = new AlertDialog.Builder(this);
										alert.SetTitle("Login Non Riuscito");
										alert.SetMessage("E-Mail o Password Errata");
										alert.SetPositiveButton("OK", (senderAlert, args) => { });
										alert.Show();
									});
						}

					});
				}

			};

			token = prefs.GetString("TokenCortexa", "");
			Console.WriteLine(token);

			if (!string.IsNullOrWhiteSpace(token)) { 
			
				FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Visible;

				var clientCheck = new RestClient("http://www.cortexa.it/");

				var requestCheck = new RestRequest("cortexa-app/banner.php?token=" + token, Method.GET);


				clientCheck.ExecuteAsync(requestCheck, (s, e) =>
				{

					Console.WriteLine("Result Check:" + s.StatusCode + "|" + s.Content);

					if (s.StatusCode == System.Net.HttpStatusCode.OK)
					{

						RunOnUiThread(() =>
						{
							LoadData();
						});

					}
					else
					{
						RunOnUiThread(() =>
						{
							FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;
						});
					}

				});

			}

		}

		static string IsTabletDevice(Context context)
		{
			try
			{

				//
				// Compute screen size
				DisplayMetrics dm = context.Resources.DisplayMetrics;
				float screenWidth = dm.WidthPixels / dm.Xdpi;
				float screenHeight = dm.HeightPixels / dm.Ydpi;
				double size = Math.Sqrt(Math.Pow(screenWidth, 2) + Math.Pow(screenHeight, 2));

				//
				// Tablet devices should have a screen size greater than 6 inches
				if (size >= 6)
					return "tablet";
				else
					return "phone";
				
			}
			catch (Exception e)
			{
				return "error";
			}
		}

		public void LoadData() { 

			//******* DOWNLOAD VIDEO *****
			var clientVideo = new RestClient("http://www.cortexa.it/");

			var requestVideo = new RestRequest("cortexa-app/video.php?token="+ token, Method.GET);


			clientVideo.ExecuteAsync(requestVideo, (s, e) =>
			{

				Console.WriteLine("Result Video:" + s.StatusCode + "|" + s.Content);

				if (s.StatusCode == System.Net.HttpStatusCode.OK)
				{

					ListVideo = JsonConvert.DeserializeObject<List<VideoObject>>(s.Content);
					LoadFiniti++;
				}

				if (LoadFiniti == LoadTotali)
				{
					RunOnUiThread(() =>
					{
						Login = true;

						FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

						Intent intent= null;
						string response = IsTabletDevice(ApplicationContext);
						if (response == "tablet")
							intent = new Intent(this, typeof(HomePageActivityTablet));
						if (response == "phone")
							intent = new Intent(this, typeof(HomePageActivity));
						if (response != "error")
						{
							FirstDownload = true;

							if (notification)
							{
								intent.PutExtra("Notification", notification);
								intent.PutExtra("type", type);
							}
							StartActivity(intent);
							//Finish();
						}

					});
				}

			});


			//***** DOWNLOAD CORSI ****
			var clientCorsi = new RestClient("http://www.cortexa.it/");

			var requestCorsi = new RestRequest("cortexa-app/corsi.php?token=" + token, Method.GET);


			clientCorsi.ExecuteAsync(requestCorsi, (s, e) =>
			{

				Console.WriteLine("Result Corsi:" + s.StatusCode + "|" + s.Content);

				if (s.StatusCode == System.Net.HttpStatusCode.OK)
				{

					ListCorsi = JsonConvert.DeserializeObject<List<CorsiObject>>(s.Content);

					RunOnUiThread(() =>
					{
						for (int i = 0; i < ListCorsi.Count; i++)
						{

							var clientCorsi2 = new RestClient("http://www.cortexa.it/");

							var requestCorsi2 = new RestRequest("cortexa-app/dettagli_corso.php?id="+ListCorsi[i].id+"&token=" + token, Method.GET);

							IRestResponse response  = clientCorsi2.Execute(requestCorsi2);

							Console.WriteLine("Result Corso "+ListCorsi[i].id+":" + response.StatusCode + "|" + response.Content);

							if (s.StatusCode == System.Net.HttpStatusCode.OK)
							{
								
								List<CorsiDettagliObject> cdo = new List<CorsiDettagliObject>();

								cdo = JsonConvert.DeserializeObject<List<CorsiDettagliObject>>(response.Content);

								ListCorsi[i].Dettagli = cdo[0];

							}

						}

					});

					LoadFiniti++;

				}

				if (LoadFiniti == LoadTotali)
				{
					RunOnUiThread(() =>
					{
						Login = true;

						FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

						Intent intent= null;
						string response = IsTabletDevice(ApplicationContext);
						if (response == "tablet")
							intent = new Intent(this, typeof(HomePageActivityTablet));
						if (response == "phone")
							intent = new Intent(this, typeof(HomePageActivity));
						if (response != "error")
						{
							FirstDownload = true;

							if (notification)
							{
								intent.PutExtra("Notification", notification);
								intent.PutExtra("type", type);
							}
							StartActivity(intent);
							//Finish();
						}

					});
				}

			});


			//***** DOWNLOAD NEWS ****
			var clientNews = new RestClient("http://www.cortexa.it/");

			var requestNews = new RestRequest("cortexa-app/news.php?token=" + token, Method.GET);


			clientNews.ExecuteAsync(requestNews, (s, e) =>
			{

				Console.WriteLine("Result News:" + s.StatusCode + "|" + s.Content);

				if (s.StatusCode == System.Net.HttpStatusCode.OK)
				{

					ListNews = JsonConvert.DeserializeObject<List<NewsObject>>(s.Content);

					LoadFiniti++;

				}

				if (LoadFiniti == LoadTotali)
				{
					RunOnUiThread(() =>
					{
						Login = true;

						FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

						Intent intent= null;
						string response = IsTabletDevice(ApplicationContext);
						if (response == "tablet")
							intent = new Intent(this, typeof(HomePageActivityTablet));
						if (response == "phone")
							intent = new Intent(this, typeof(HomePageActivity));
						if (response != "error")
						{
							FirstDownload = true;

							if (notification)
							{
								intent.PutExtra("Notification", notification);
								intent.PutExtra("type", type);
							}
							StartActivity(intent);
							//Finish();
						}

					});
				}

			});


			//***** DOWNLOAD MANUALE ****
			var clientManuale = new RestClient("http://www.cortexa.it/");

			var requestManuale = new RestRequest("cortexa-app/manuale.php?token=" + token, Method.GET);


			clientManuale.ExecuteAsync(requestManuale, (s, e) =>
			{

				Console.WriteLine("Result Manuale:" + s.StatusCode + "|" + s.Content);

				if (s.StatusCode == System.Net.HttpStatusCode.OK)
				{

					ListManuale = JsonConvert.DeserializeObject<List<ManualeObject>>(s.Content);

					LoadFiniti++;

				}

				if (LoadFiniti == LoadTotali)
				{
					RunOnUiThread(() =>
					{
						Login = true;

						FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

						Intent intent= null;
						string response = IsTabletDevice(ApplicationContext);
						if (response == "tablet")
							intent = new Intent(this, typeof(HomePageActivityTablet));
						if (response == "phone")
							intent = new Intent(this, typeof(HomePageActivity));
						if (response != "error")
						{
							FirstDownload = true;

							if (notification)
							{
								intent.PutExtra("Notification", notification);
								intent.PutExtra("type", type);
							}
							StartActivity(intent);
							//Finish();
						}

					});
				}

			});

			//***** DOWNLOAD PARTNERS ****
			var clientPartners = new RestClient("http://www.cortexa.it/");

			var requestPartners = new RestRequest("cortexa-app/partner.php?token=" + token, Method.GET);


			clientPartners.ExecuteAsync(requestPartners, (s, e) =>
			{

				Console.WriteLine("Result Partners:" + s.StatusCode + "|" + s.Content);

				if (s.StatusCode == System.Net.HttpStatusCode.OK)
				{

					ListPartners = JsonConvert.DeserializeObject<List<PartnersObject>>(s.Content);

					LoadFiniti++;

				}

				if (LoadFiniti == LoadTotali)
				{
					RunOnUiThread(() =>
					{
						Login = true;
					
						FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

						Intent intent= null;
						string response = IsTabletDevice(ApplicationContext);
						if(response == "tablet")
							intent = new Intent(this, typeof(HomePageActivityTablet));
						if(response == "phone")
							intent = new Intent(this, typeof(HomePageActivity));
						if (response != "error")
						{
							FirstDownload = true;

							if (notification)
							{
								intent.PutExtra("Notification", notification);
								intent.PutExtra("type", type);
							}
							StartActivity(intent);
							//Finish();
						}

					});
				}

			});

			//***** DOWNLOAD BANNER ****
			var clientBanner = new RestClient("http://www.cortexa.it/");

			var requestBanner = new RestRequest("cortexa-app/banner.php?token=" + token, Method.GET);


			clientBanner.ExecuteAsync(requestBanner, (s, e) =>
			{

				Console.WriteLine("Result Banner:" + s.StatusCode + "|" + s.Content);

				if (s.StatusCode == System.Net.HttpStatusCode.OK)
				{

					ListBanner = JsonConvert.DeserializeObject<List<BannerObject>>(s.Content);

					LoadFiniti++;

				}

				if (LoadFiniti == LoadTotali)
				{
					RunOnUiThread(() =>
					{
						Login = true;

						FindViewById<RelativeLayout>(Resource.Id.LoadView).Visibility = ViewStates.Gone;

						Intent intent = null;
						string response = IsTabletDevice(ApplicationContext);
						if (response == "tablet")
							intent = new Intent(this, typeof(HomePageActivityTablet));
						if (response == "phone")
							intent = new Intent(this, typeof(HomePageActivity));
						if (response != "error")
						{
							FirstDownload = true;

							if (notification)
							{
								intent.PutExtra("Notification", notification);
								intent.PutExtra("type", type);
							}
							StartActivity(intent);
							//Finish();
						}

					});
				}

			});
		}
	}
}


