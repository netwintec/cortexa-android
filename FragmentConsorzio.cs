﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentConsorzio : Fragment
	{

		static string TXT_CHISIAMO = "Nel luglio 2007 nasce CORTEXA, il Consorzio per la cultura del Sistema a Cappotto.\n\n\nCORTEXA, il consorzio italiano per la cultura del Sistema a Cappotto, unisce sotto lo stesso marchio le più grandi aziende del settore sfruttando la loro esperienza trentennale.\n\nIl Consorzio si propone di diffondere la cultura dell'isolamento a cappotto, mettendo a disposizione le conoscenze delle aziende associate per assicurare al mercato italiano un alto standard tecnologico finalizzato al conseguimento di obiettivi come risparmio energetico e vantaggi economici, termici, strutturali e di durata nel tempo.\n\nL'esperienza del gruppo di aziende consorziate garantisce controlli completi sui singoli componenti e sull'applicazione dei prodotti, consulenze tecniche qualificate e la continua assistenza in cantiere.\n\nIl Consorzio fornisce inoltre una formazione continua agli specialisti del settore, per diffondere la conoscenza e offrire qualità anche nella fase diagnostica, progettuale ed esecutiva dell'installazione del Sistema a Cappotto.\n\nCortexa è socio fondatore di EAE, l'Associazione Europea per il Sistema di Isolamento a Cappotto.";
		static string TXT_CONSORZIATI = "Tutte le aziende associate a Cortexa vantano più di 30 anni di esperienza nel settore e milioni di metri quadri di facciate di edifici isolati in Italia e nel mondo, perché solo l’esperienza e il know-how di chi è da tempo sul mercato può garantire affidabilità e qualità. \nLe aziende fondatrici, fortemente specializzate nel settore della protezione termica integrale e costantemente impegnate ad investire in formazione e ricerca nel campo dell’isolamento termico in edilizia, migliorano continuamente i propri prodotti e la propria offerta al mercato.\nOffrono un servizio di consulenza e di assistenza specializzata sul cantiere, oltre ad una gamma completa ed organizzata di sistemi professionali per l’isolamento termico.\n\nAderire a Cortexa significa per le aziende consorziate continuare il proprio percorso di miglioramento delle prestazioni e di garanzia della qualità.\n\nSoci Ordinari:\n\n\t●Alligator Italia\n\t●Baumit Italia\n\t●Caparol Italiana\n\t●Ivas - Industria Vernici\n\t●Röfix\n\t●Settef\n\t●Sigma Coatings\n\t●Sto Italia\n\t●Viero\n\t●Waler\n\n\nMain Partner:\n\n\t●BASF\n\t●Saint Gobain - Isover\n\t●Knauf Insulation\n\t●Rockwool\n\t●Stiferite\n\t●Eni Versalis\n\n\nPartner:\n\n\t\t●EJOT\n\n\nPartner Tecnico:\n\n\t\t●AIPE\n\t\t●FIVRA";
		static string TXT_PERCHECONSORZIO = "Il Consorzio Cortexa è il soggetto referente protagonista istituzionale della cultura del Sistema a Cappotto, organizzato e composto da più aziende con comprovata esperienza pluriennale che condividono una etica professionale, competenze tecniche specialistiche ed un progetto con finalità comuni.\n\nCortexa nasce con l'obiettivo di diffondere in Italia la cultura del Sistema di Isolamento a Cappotto di qualità, sia in termini di sistema che di progettazione e applicazione.";

		bool FirstOpen = false;
		bool SecondOpen = false;
		bool ThirdOpen = false;

		RelativeLayout Click1, Click2, Click3, Exp1, Exp2, Exp3;
		ImageView imm1, imm2, imm3;
		TextView tx1, tx2, tx3, tx4, tx5, tx6;

		int plus = Resource.Drawable.Plus;
		int minus = Resource.Drawable.Minus;

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentConsorzioLayout, container, false);


			Typeface osr = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Regular.ttf");
			Typeface osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");

			Click1 = view.FindViewById<RelativeLayout>(Resource.Id.ClickableView1);
			imm1 = view.FindViewById<ImageView>(Resource.Id.imageView1);
			tx1 = view.FindViewById<TextView>(Resource.Id.textView1);

			Exp1 = view.FindViewById<RelativeLayout>(Resource.Id.ExpandableView1);
			tx2 = view.FindViewById<TextView>(Resource.Id.textView2);

			Click2 = view.FindViewById<RelativeLayout>(Resource.Id.ClickableView2);
			imm2 = view.FindViewById<ImageView>(Resource.Id.imageView2);
			tx3 = view.FindViewById<TextView>(Resource.Id.textView3);

			Exp2 = view.FindViewById<RelativeLayout>(Resource.Id.ExpandableView2);
			tx4 = view.FindViewById<TextView>(Resource.Id.textView4);

			Click3 = view.FindViewById<RelativeLayout>(Resource.Id.ClickableView3);
			imm3 = view.FindViewById<ImageView>(Resource.Id.imageView3);
			tx5 = view.FindViewById<TextView>(Resource.Id.textView5);

			Exp3 = view.FindViewById<RelativeLayout>(Resource.Id.ExpandableView3);
			tx6 = view.FindViewById<TextView>(Resource.Id.textView6);


			Exp1.Visibility = ViewStates.Gone;
			Exp2.Visibility = ViewStates.Gone;
			Exp3.Visibility = ViewStates.Gone;

			tx2.Text = TXT_CHISIAMO;
			tx4.Text = TXT_CONSORZIATI;
			tx6.Text = TXT_PERCHECONSORZIO;

			tx1.Typeface = osr;
			tx2.Typeface = osr;
			tx3.Typeface = osr;
			tx4.Typeface = osr;
			tx5.Typeface = osr;
			tx6.Typeface = osr;

			Click1.Click+= delegate {

				ControllView(1);

			};

			Click2.Click += delegate
			{

				ControllView(2);

			};

			Click3.Click += delegate
			{

				ControllView(3);

			};
			return view;
		}

		public void ControllView(int i) {

			if (i == 1) {
				if (FirstOpen)
				{

					Exp1.Visibility = ViewStates.Gone;
					imm1.SetImageResource(plus);
					FirstOpen = false;

				}
				else{
					
					Exp1.Visibility = ViewStates.Visible;
					imm1.SetImageResource(minus);
					FirstOpen = true;

					if (SecondOpen)
					{

						Exp2.Visibility = ViewStates.Gone;
						imm2.SetImageResource(plus);
						SecondOpen = false;

					}

					if (ThirdOpen)
					{

						Exp3.Visibility = ViewStates.Gone;
						imm3.SetImageResource(plus);
						ThirdOpen = false;

					}
				}	
			}

			if (i == 2)
			{
				if (SecondOpen)
				{

					Exp2.Visibility = ViewStates.Gone;
					imm2.SetImageResource(plus);
					SecondOpen = false;

				}
				else {

					Exp2.Visibility = ViewStates.Visible;
					imm2.SetImageResource(minus);
					SecondOpen = true;

					if (FirstOpen)
					{

						Exp1.Visibility = ViewStates.Gone;
						imm1.SetImageResource(plus);
						FirstOpen = false;

					}

					if (ThirdOpen)
					{

						Exp3.Visibility = ViewStates.Gone;
						imm3.SetImageResource(plus);
						ThirdOpen = false;

					}
				}
			}

			if (i == 3)
			{
				if (ThirdOpen)
				{

					Exp3.Visibility = ViewStates.Gone;
					imm3.SetImageResource(plus);
					ThirdOpen = false;

				}
				else {

					Exp3.Visibility = ViewStates.Visible;
					imm3.SetImageResource(minus);
					ThirdOpen = true;

					if (SecondOpen)
					{

						Exp2.Visibility = ViewStates.Gone;
						imm2.SetImageResource(plus);
						SecondOpen = false;

					}

					if (FirstOpen)
					{

						Exp1.Visibility = ViewStates.Gone;
						imm1.SetImageResource(plus);
						FirstOpen = false;

					}
				}
			}
		
		}
	}
}

