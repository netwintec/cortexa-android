﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentNewsDettaglio : Fragment
	{

		NewsObject no;

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentNewsDettaglioLayout, container, false);

			LinearLayout l1 = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);

			no = FragmentNews.Instance.NewsSelected;

			Typeface osr = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Regular.ttf");
			Typeface osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");

			ImageView imm = view.FindViewById<ImageView>(Resource.Id.NewsImage);
			TextView tit = view.FindViewById<TextView>(Resource.Id.TitleText);
			TextView data = view.FindViewById<TextView>(Resource.Id.DataText);
			TextView desc = view.FindViewById<TextView>(Resource.Id.DescText);

			if (no.Image == null)
			{
				caricaImmagineAsync(no.immagine, imm, no);
			}
			else {
				imm.SetImageBitmap(no.Image);
			}

			tit.Text = no.titolo;
			data.Text = no.data;
			desc.Text = no.testo;

			tit.Typeface = osr;
			data.Typeface = osb;
			desc.Typeface = osr;

			return view;
		}

		async void caricaImmagineAsync(String uri, ImageView immagine_view, NewsObject obj)
		{
			WebClient webClient = new WebClient();
			byte[] bytes = null;
			try
			{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch (TaskCanceledException)
			{
				Console.WriteLine("Task Canceled!**************************************");
				return;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap(immagine);
			obj.Image = immagine;

			Console.WriteLine("Immagine caricata!");

		}
	}
}

