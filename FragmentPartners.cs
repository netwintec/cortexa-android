﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentPartners : Fragment
	{
		public List<PartnersObject> ListPartners;

		public PartnersObject PartnerSelected;

		public static FragmentPartners Instance { private set; get; }

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentPartnersLayout, container, false);

			FragmentPartners.Instance = this;

			LinearLayout l1 = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);

			ListPartners = MainActivity.Instance.ListPartners;


			for (int i = 0; i < ListPartners.Count; i++)
			{

				PartnersObject po = ListPartners[i];

				View PartnersView = inflater.Inflate(Resource.Layout.PartnerObjectLayout, container, false);

				ImageView imm = PartnersView.FindViewById<ImageView>(Resource.Id.imageView1);

				//imm.SetScaleType(ImageView.ScaleType.);

				if (po.Image == null)
				{
					caricaImmagineAsync(po.immagine, imm, po);
				}
				else {
					imm.SetImageBitmap(po.Image);
				}

				int indice = i;

				imm.Click += delegate
				{

					Console.WriteLine(indice + "!" + i);

					PartnerSelected = ListPartners[indice];

					FragmentPartnersDettaglio fragment = new FragmentPartnersDettaglio();

					FragmentManager frgManager = this.FragmentManager;
					frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();

				};


				l1.AddView(PartnersView);


			}


			return view;
		}

		private int ConvertDpToPixel(float dp)
		{
			var pixel = (int)(dp * Resources.DisplayMetrics.Density);
			return pixel;
		}

		async void caricaImmagineAsync(String uri, ImageView immagine_view, PartnersObject obj)
		{
			WebClient webClient = new WebClient();
			byte[] bytes = null;
			try
			{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch (TaskCanceledException)
			{
				Console.WriteLine("Task Canceled!**************************************");
				return;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap(immagine);
			obj.Image = immagine;

			Console.WriteLine("Immagine caricata!");

		}
	}
}

