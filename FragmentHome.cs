﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentHome : Fragment
	{

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentHomeLayout_v2, container, false);

			Typeface osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");


			ImageView ConsorzioButton = view.FindViewById<ImageView>(Resource.Id.ConsorzioButton);
			ImageView NewsButton = view.FindViewById<ImageView>(Resource.Id.NewsButton);
			ImageView VideoButton = view.FindViewById<ImageView>(Resource.Id.VideoButton); 
			ImageView WebinarButton = view.FindViewById<ImageView>(Resource.Id.WebinarButton);
			ImageView PartnerButton = view.FindViewById<ImageView>(Resource.Id.PartnerButton);
			ImageView ManualeButton = view.FindViewById<ImageView>(Resource.Id.ManualeButton);

			TextView ConsorzioText = view.FindViewById<TextView>(Resource.Id.ConsorzioText);
			TextView NewsText = view.FindViewById<TextView>(Resource.Id.NewsText);
			TextView VideoText = view.FindViewById<TextView>(Resource.Id.VideoText);
			TextView WebinarText = view.FindViewById<TextView>(Resource.Id.WebinarText);
			TextView PartnerText = view.FindViewById<TextView>(Resource.Id.PartnerText);
			TextView ManualeText = view.FindViewById<TextView>(Resource.Id.ManualeText);

			ConsorzioText.TextSize = 15;
			ConsorzioText.Typeface = osb;

			NewsText.TextSize = 15;
			NewsText.Typeface = osb;

			VideoText.TextSize = 15;
			VideoText.Typeface = osb;

			WebinarText.TextSize = 15;
			WebinarText.Typeface = osb;

			PartnerText.TextSize = 15;
			PartnerText.Typeface = osb;

			ManualeText.TextSize = 15;
			ManualeText.Typeface = osb;

			ConsorzioButton.Click += delegate
			{
				ClickListener(0);
			};

			NewsButton.Click += delegate
			{
				ClickListener(1);
			};

			VideoButton.Click += delegate
			{
				ClickListener(2);
			};

			WebinarButton.Click += delegate
			{
				ClickListener(3);
			};

			PartnerButton.Click += delegate
			{
				ClickListener(4);
			};

			ManualeButton.Click += delegate
			{
				ClickListener(5);
			};



			return view;
			//return base.OnCreateView(inflater, container, savedInstanceState);
		}

		public void ClickListener(int button)
		{
			Fragment fragment = null;
			bool isFragment = false;

			switch (button)
			{
				case 0:
					fragment = new FragmentConsorzio();
					isFragment = true;
					break;
				case 1:
					fragment = new FragmentNews();
					isFragment = true;
					break;
				case 2:
					fragment = new FragmentVideo();
					isFragment = true;
					break;
				case 3:
					fragment = new FragmentCorsi();
					isFragment = true;
					break;
				case 4:
					fragment = new FragmentPartners();
					isFragment = true;
					break;
				case 5:
					fragment = new FragmentManuale();
					isFragment = true;
					break;
				default:
					break;
			}
			if (isFragment)
			{
				FragmentManager frgManager = this.FragmentManager;
				frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
					.AddToBackStack(null)
					.Commit();
			}
		}
	}
}

