﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentCorsiDettaglio : Fragment
	{
		CorsiObject co;


		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentCorsiDettaglioLayout, container, false);

			LinearLayout l1 = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);

			co = FragmentCorsi.Instance.CorsoSelected;

			Typeface osr = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Regular.ttf");
			Typeface osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");

			TextView cor = view.FindViewById<TextView>(Resource.Id.CorsoText);
			TextView tit = view.FindViewById<TextView>(Resource.Id.TitleText);
			TextView desc = view.FindViewById<TextView>(Resource.Id.DescText);
			TextView tv1 = view.FindViewById<TextView>(Resource.Id.textView1);
			TextView tv2 = view.FindViewById<TextView>(Resource.Id.textView2);
			TextView req = view.FindViewById<TextView>(Resource.Id.ReqText);

			Button leggi = view.FindViewById<Button>(Resource.Id.LeggiButton);
			Button chiudi = view.FindViewById<Button>(Resource.Id.ChiudiButton);

			RelativeLayout iscrizione = view.FindViewById<RelativeLayout>(Resource.Id.IscrivitiView);
			RelativeLayout PopUp = view.FindViewById<RelativeLayout>(Resource.Id.PopUpView);

			cor.Text = co.Dettagli.numero;
			tit.Text = co.Dettagli.titolo;
			desc.Text = co.Dettagli.descrizione;
			req.Text = co.Dettagli.requisiti;

			cor.Typeface = osb;
			tit.Typeface = osr;
			desc.Typeface = osr;
			tv1.Typeface = osb;
			tv2.Typeface = osr;
			leggi.Typeface = osr;
			req.Typeface = osr;
			chiudi.Typeface = osr;

			PopUp.Visibility = ViewStates.Gone;
			if(co.abilitato == "0")
				iscrizione.Visibility = ViewStates.Gone;

			leggi.Click += delegate {

				PopUp.Visibility = ViewStates.Visible;

			};

			chiudi.Click += delegate
			{

				PopUp.Visibility = ViewStates.Gone;

			};

			iscrizione.Click += delegate {
				
				var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(co.Dettagli.url_iscrizione));
				intent.AddFlags(ActivityFlags.NewTask);

				Application.Context.StartActivity(intent);

			};

			return view;
		}

	}
}

