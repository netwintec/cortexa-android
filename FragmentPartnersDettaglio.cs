﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentPartnersDettaglio : Fragment
	{
		PartnersObject po;

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentPartnersDettaglioLayout, container, false);

			LinearLayout l1 = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);

			po = FragmentPartners.Instance.PartnerSelected;

			Typeface osr = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Regular.ttf");
			Typeface osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");

			ImageView imm = view.FindViewById<ImageView>(Resource.Id.imageView1);
			TextView nom = view.FindViewById<TextView>(Resource.Id.NomeText);
			TextView descr = view.FindViewById<TextView>(Resource.Id.DescrText);
			Button sito = view.FindViewById<Button>(Resource.Id.SitoButton);
			Console.WriteLine(3+"|"+ (imm == null));
			if (po.Image == null)
			{
				caricaImmagineAsync(po.immagine, imm, po);
			}
			else {
				imm.SetImageBitmap(po.Image);
			}
			Console.WriteLine(4);
			nom.Text = po.nome;
			descr.Text = po.descrizione;
			Console.WriteLine(5);
			nom.Typeface = osr;
			descr.Typeface = osr;
			sito.Typeface = osr;
			Console.WriteLine(6);
			sito.Click += delegate {

				var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(po.sito_web));
				intent.AddFlags(ActivityFlags.NewTask);

				Application.Context.StartActivity(intent);

			};
			Console.WriteLine(7);
			return view;
		}

		async void caricaImmagineAsync(String uri, ImageView immagine_view, PartnersObject obj)
		{
			WebClient webClient = new WebClient();
			byte[] bytes = null;
			try
			{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch (TaskCanceledException)
			{
				Console.WriteLine("Task Canceled!**************************************");
				return;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap(immagine);
			obj.Image = immagine;

			Console.WriteLine("Immagine caricata!");

		}
	}
}

