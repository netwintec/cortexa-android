﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Webkit;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentVideo : Fragment
	{

		int intDisplayWidth;
		int intDisplayHeight;

		List<WebView> ListWebView = new List<WebView>();
		List<string> ListUrl = new List<string>();

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			//Activity.Window.RequestFeature(Android.Views.WindowFeatures.NoTitle);

			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentVideoLayout, container, false);


			Typeface osr = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Regular.ttf");


			//******************  YOUTUBE VIDEO EMBEDDED **************

			var metrics = Resources.DisplayMetrics;

			//fix video screen height and width
			intDisplayWidth = FnConvertPixelsToDp(metrics.WidthPixels);
			intDisplayHeight = (int)((intDisplayWidth/16)*9);

			LinearLayout l1 = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);
			l1.SetGravity(GravityFlags.CenterHorizontal);

			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
			lp1.SetMargins(0,ConvertDpToPixel(3),0,ConvertDpToPixel(3));
			LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
			lp2.SetMargins(ConvertDpToPixel(20),ConvertDpToPixel(5),ConvertDpToPixel(20),ConvertDpToPixel(5));
			LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
			lp3.SetMargins(ConvertDpToPixel(20), ConvertDpToPixel(0), ConvertDpToPixel(20), ConvertDpToPixel(5));

			foreach (var videoObj in MainActivity.Instance.ListVideo)
			{
				ListUrl.Add(videoObj.url);
				Console.WriteLine(videoObj.id + "|" + videoObj.url + "|" + videoObj.titolo);
			}

			int i = 0;
			foreach (string url in ListUrl) {

				LinearLayout Container = new LinearLayout(Activity);
				Container.LayoutParameters = lp1;
				Container.SetBackgroundColor(Android.Graphics.Color.White);
				Container.SetGravity(GravityFlags.CenterHorizontal);
				Container.Orientation = Orientation.Vertical;

				WebView web = new WebView(Activity);
				web.LayoutParameters = lp2;
				web.SetBackgroundColor(Android.Graphics.Color.White);

				TextView tit = new TextView(Activity);
				tit.LayoutParameters = lp3;
				tit.SetBackgroundColor(Android.Graphics.Color.White);
				tit.Text = MainActivity.Instance.ListVideo[i].titolo;
				tit.SetTextColor (Android.Graphics.Color.Black);
				tit.TextSize = 18;
				tit.Typeface = osr;

				Container.AddView(web);
				Container.AddView(tit);
				l1.AddView(Container);

				ListWebView.Add(web);

				i++;
			}

			FnPlayInWebView();

			return view;
		}

		/*******************  START YOUTUBE VIDEO EMBEDDED ********************/
		void FnPlayInWebView()
		{


			for (int i = 0; i < ListWebView.Count; i++)
			{
				string strUrl = ListUrl[i];

				string id = FnGetVideoID(strUrl);

				if (!string.IsNullOrEmpty(id))
				{
					strUrl = string.Format("http://youtube.com/embed/" + id);
				}
				else
				{
					Toast.MakeText(Activity, "Video url is not in correct format", ToastLength.Long).Show();
					return;
				}

				string html = @"<html><body style=""margin:0!important;padding:0!important""><iframe width=""videoWidth"" height=""videoHeight"" src=""strUrl""></iframe></body></html>";
				WebView myWebView = ListWebView[i];
				var settings = myWebView.Settings;
				settings.JavaScriptEnabled = true;
				settings.UseWideViewPort = true;
				settings.LoadWithOverviewMode = true;
				settings.JavaScriptCanOpenWindowsAutomatically = true;
				settings.DomStorageEnabled = true;
				settings.SetRenderPriority(WebSettings.RenderPriority.High);
				settings.BuiltInZoomControls = false;

				settings.JavaScriptCanOpenWindowsAutomatically = true;
				myWebView.SetWebChromeClient(new WebChromeClient());
				settings.AllowFileAccess = true;
				settings.SetPluginState(WebSettings.PluginState.On);
				string strYouTubeURL = html.Replace("videoWidth", intDisplayWidth.ToString()).Replace("videoHeight", intDisplayHeight.ToString()).Replace("strUrl", strUrl);

				myWebView.LoadDataWithBaseURL(null, strYouTubeURL, "text/html", "UTF-8", null);
			}
		}

		int FnConvertPixelsToDp(float pixelValue)
		{
			var dp = (int)((pixelValue) / Resources.DisplayMetrics.Density);
			return dp;
		}

		static string FnGetVideoID(string strVideoURL)
		{
			const string regExpPattern = @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
			//for Vimeo: vimeo\.com/(?:.*#|.*/videos/)?([0-9]+)
			var regEx = new Regex(regExpPattern);
			var match = regEx.Match(strVideoURL);
			return match.Success ? match.Groups[1].Value : null;
		}

		private int ConvertDpToPixel(float dp)
		{
			var pixel = (int)(dp * Resources.DisplayMetrics.Density);
			return pixel;
		}

		/*******************  END YOUTUBE VIDEO EMBEDDED ********************/
	}
}

