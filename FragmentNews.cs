﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Cortexa
{
	public class FragmentNews : Fragment
	{
		List<NewsObject> ListNews;

		public NewsObject NewsSelected;

		public static FragmentNews Instance { private set; get; }

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			//Activity.Window.RequestFeature(Android.Views.WindowFeatures.NoTitle);

			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentNewsLayout, container, false);

			LinearLayout l1 = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);

			FragmentNews.Instance = this;

			ListNews = MainActivity.Instance.ListNews;

			Typeface osr = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Regular.ttf");
			Typeface osb = Typeface.CreateFromAsset(Activity.Assets, "fonts/OpenSans_Bold.ttf");

			for (int i = 0; i < ListNews.Count; i++) {

				NewsObject no = ListNews[i];

				View NewsView = inflater.Inflate(Resource.Layout.NewsObjectLayout, container, false);

				ImageView imm = NewsView.FindViewById<ImageView>(Resource.Id.NewsImage);
				TextView tit = NewsView.FindViewById<TextView>(Resource.Id.TitleText);
				TextView data =NewsView.FindViewById<TextView>(Resource.Id.DataText);
				TextView desc = NewsView.FindViewById<TextView>(Resource.Id.DescText);

				if (no.Image == null) {
					caricaImmagineAsync(no.immagine, imm, no);
				}
				else {
					imm.SetImageBitmap(no.Image);
				}

				tit.Text = no.titolo;
				data.Text = no.data;
				desc.Text = no.testo;

				tit.Typeface = osr;
				data.Typeface = osb;
				desc.Typeface = osr;

				int indice = i;

				NewsView.Click += delegate
				{

					Console.WriteLine(indice + "!" + i);

					NewsSelected = ListNews[indice];

					FragmentNewsDettaglio fragment = new FragmentNewsDettaglio();

					FragmentManager frgManager = this.FragmentManager;
					frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();

				};

				l1.AddView(NewsView);

			}

			return view;
		}

		async void caricaImmagineAsync(String uri, ImageView immagine_view, NewsObject obj)
		{
			WebClient webClient = new WebClient();
			byte[] bytes = null;
			try
			{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch (TaskCanceledException)
			{
				Console.WriteLine("Task Canceled!**************************************");
				return;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap(immagine);
			obj.Image = immagine;

			Console.WriteLine("Immagine caricata!");

		}

	}
}

